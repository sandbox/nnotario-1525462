<?php

/**
 * @file
 * Makes group widget a better widget and similar to "tagging" widget.
 * It's heavily based in tagging module
 */

/**
 * Implements hook_theme().
 */
function nice_groups_theme() {
  $theme = array(
    'grouping_widget_button' => array('arguments' => array('vid' => NULL)),
    'grouping_widget_wrapper' => array('arguments' => array('content' => NULL, 'vid' => NULL)),
    'grouping_tags_list' => array('arguments' => array('tags' => array(), 'vid' => NULL)),
  );
  return $theme;
}

/**
 * hook_form_alter
 * Alters a node edit form
 *
 * @param type $form
 *   The form
 * @param type $form_state
 *   The form state
 * @param type $form_id
 *   The form id
 */
function nice_groups_form_alter(&$form, &$form_state, $form_id) {
  if (isset($form['#node']) && $form_id == $form['#node']->type . '_node_form') {
    $node = $form['#node'];
    if (og_is_group_post_type($node->type)) {

      // Moves the og_nodeapi fields out of the og_nodeapi fieldset.
      $form['og_nodeapi_nicegroups_invisible'] = $form['og_nodeapi']['visible'];
      unset($form['og_nodeapi']['visible']);

      $form['og_nodeapi_nicegroups_invisible']['og_public']['#weight'] = 10;
      // Creates javascript settings with the options and the selected values.
      $available_groups = array(
        'options' => $form['og_nodeapi_nicegroups_invisible']['og_groups']['#options'],
        'selected' => $form['og_nodeapi_nicegroups_invisible']['og_groups']['#default_value'],
      );

      // Creates a fieldset.
      $form['og_nodeapi_nicegroups'] = array(
        '#type' => 'fieldset',
        '#group' => FALSE,
        '#title' => t('Groups'),
        '#weight' => 0,
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      );

      // Imitates the tagging module.
      $form['og_nodeapi_nicegroups']['input_field'] = array(
        '#type' => 'item',
        '#attributes' => array(
          'class' => array(
            'grouping-widget-input-wrapper',
            'clearfix',
          ),
        ),
      );

      // Some markup for the "group tags" container.
      $form['og_nodeapi_nicegroups']['input_field']['wrapper'] = array(
        '#value' => theme('grouping_widget_wrapper'),
      );


      // Copies the multiple group select to our new fieldset.
      $form['og_nodeapi_nicegroups']['input_field']['og_group_hidden_select'] = $form['og_nodeapi_nicegroups_invisible']['og_groups'];
      $form['og_nodeapi_nicegroups']['input_field']['og_group_hidden_select']['#attributes'] = array(
        'class' => 'grouping-widget-input-og-groups clearfix',
      );

      // Makes the select a not multiple one.
      $form['og_nodeapi_nicegroups']['input_field']['og_group_hidden_select']['#multiple'] = FALSE;
      $form['og_nodeapi_nicegroups']['input_field']['og_group_hidden_select']['#description'] = NULL;
      $form['og_nodeapi_nicegroups']['input_field']['og_group_hidden_select']['#title'] = t('Add group');
      // Adds the add button.
      $form['og_nodeapi_nicegroups']['input_field']['button'] = array(
        '#value' => theme('grouping_widget_button'),
      );
      // Moves public checkbox into our new fieldset.
      $form['og_nodeapi_nicegroups']['input_field']['og_public'] = $form['og_nodeapi_nicegroups_invisible']['og_public'];
      unset($form['og_nodeapi_nicegroups_invisible']['og_public']);

      // Adds javascript settings (available groups and selected ones)
      drupal_add_js(array('nice_groups' => $available_groups), 'setting');

      // Adds standard css and javascript.
      drupal_add_css(drupal_get_path('module', 'nice_groups') . '/css/nice_groups.css');
      drupal_add_js(drupal_get_path('module', 'nice_groups') . '/js/groups.plugin.js');
      drupal_add_js(drupal_get_path('module', 'nice_groups') . '/js/groups.init.d6.js');
    }
  }
}

/**
 * Rendering the widget add button
 */
function theme_grouping_widget_button() {
  return '<a class="grouping-button-container" href="#" title="' . t('Add') . '"><span class="grouping-button grouping-button-og-groups"></span></a>';
}

/**
 * Theming the widget wrapper
 * @return string
 *   The html
 */
function theme_grouping_widget_wrapper() {
  return '<div class="grouping-curtags-wrapper grouping-curtags-wrapper-og-groups"><label>Assigned groups:</label></div><div class="grouping-widget-input-wrapper clearfix"><div class="form-item" id="grouping-widget-input-group-wrapper"></div></div>';
}
