/**
 * @author: Nicolás Notario (http://www.nivaria.com)
 * @Copyright 2012
 * Based on
 *    Author: Eugen Mayer (http://kontextwork.de)
 *    Copyright 2010
 *    http://drupal.org/project/tagging
 **/
(function($) {
  $.fn.groups = function(options) {
    return this.each(function(){
      // **************** Init *****************/
      var context = 'og-groups';
      if(context === null) {
        alert('cant initialize grouping-widget: "'+$(this).attr('id')+'"..did you forget the "taggig-widget-$CONTEXT" class?');
        return;
      }
      // Our containers.
      var input_sel = '.grouping-widget-input-'+context;
      var button_sel = '.grouping-button-'+context;
      var wrapper_sel = '.grouping-curtags-wrapper-'+context;
      var suggestions_wrapper_sel = '.grouping-suggestions-wrapper-'+context;
      var target_sel = '#edit-og-groups';
      var suggest_class = 'grouping-suggest-tag';
      var tag_class = 'grouping-tag';
      var suggest_sel = '.'+suggest_class;
      var tag_sel =  '.'+tag_class;

      // Lets set all things up.
      bind_taglist_events();
      bind_button();
      update_tags();
      bind_enter();
      check_dublicates();
      for(var i in options.selected) {
        add_tag(options.options[options.selected[i]], true);
      }
      bind_taglist_events();
      $(input_sel).val('');
      $(this).addClass('grouping-processed');
      // **************** Helper methods *****************/
      /*
      * Adds a tag to the visual list and to the hidden input field (target).
      */
      function add_tag(tag, autoupdate) {
        tag = Drupal.checkPlain(tag);
        $(wrapper_sel).append("<span class='"+tag_class+"'>"+tag+"</span>");
        if(autoupdate) {
          update_tags();
        }
      }

      /*
      * Removes a tag out of the visual list and out of the hidden input field (target).
      */
      function remove_tag(e) {
        $(e).remove();
        unbind_taglist_events();
        update_tags();
        bind_taglist_events();
      }

      /*
      * Hides a tag out of the visual list. Suggestions need this to restore later
      */
      function hide_tag(e) {
        $(e).hide();
        unbind_taglist_events();
        update_tags();
        bind_taglist_events();
      }

      /*
      * Updates the hidden input textfield with current tags.
      * We do so, that we later can pass the tags to the taxonomy validators
      * and dont have to fight with module weights.
      */
      function update_tags() {
        var tags = new Array();
        $(wrapper_sel+' '+tag_sel).each(function () {
          for (var tag_id in options.options) {
            if (options.options[tag_id] == $(this).text()) {
              tags.push(tag_id);
            }
          }
        });
        $(target_sel).val(tags);
      }

      /*
      * Checks, if the tag already exists. Lets avoid the dublicates
      * we have seen in the past. We dont tell the use anything, we
      * just do as we would have added it, as the user expects to have the tag
      * added, no matter its there or not.
      */
      function tag_exists(tag) {
        var tag = $.trim(tag.toLowerCase());
        var found = false;
        $(wrapper_sel+' '+tag_sel).each(function() {
          if($.trim($(this).text().toLowerCase()) == tag) {
            found = true;
            return;
          }
        });
        return found;
      }


      /*
      * Adds the button to the inputfield. Actuall the button is optional
      * as we also add (primary) by pressing enter.
      */
      function bind_button() {
        $(button_sel).bind('click',function() {
          tags = $(input_sel).not('.tag-processed').val().split(',');
          $.each(tags, function(i, tag) {
            tag = jQuery.trim(tag);
            tag = options.options[tag];
            if (tag != '' && !tag_exists(tag)) {
              add_tag(tag, false);
            }
          });
          $(input_sel).val('');
          update_tags();
          bind_taglist_events();
          return false;
        });
      }

      /*
      * Event for keypress on the input field of the grouping-widget.
      */
      function bind_enter() {
        if ($.browser.mozilla || $.browser.opera) {
          $(input_sel).bind('keypress',check_enter);
        }
        else {
          $(input_sel).bind('keydown',check_enter);
        }
      }

     /*
      * Checks, if enter is pressed. If yes, close the autocompletition
      * use the selected item and add it to the tag-list.
      */
      function check_enter(event) {
        var key = event.which;
        if (key == 13) {
          $('#autocomplete').each(function() {
            this.owner.hidePopup();
          });
          $(button_sel).trigger('click');
          event.preventDefault();
          return false;
        }
        return true;
      }

      /*
      * Check for dupblicates in suggestions and allready assgined tags.
      * Hide suggestions on match.
      */
      function check_dublicates(){
        // TODO: Using this optimized selector somehow interfers with the
        // fckeditor as a module. Yet no idea what happens.
        // sel = suggestions_wrapper_sel + ' div' + suggest_sel + ":visble";

        // Fallback selector
        sel = suggestions_wrapper_sel + ' span' + suggest_sel;
        $(sel).each(function(){
          if(tag_exists($(this).text())) {
            $(this).hide();
          }
        });
      }

     /*
      * Adds the remove-tag methods to the tags in the wrapper.
      */
      function bind_taglist_events() {
        $(wrapper_sel+' span'+tag_sel+':not(span.processed)').each(function() {
          $(this).addClass('processed');
          // We use non anonymuos binds to be properly able to unbind them.
          $(this).bind('click',remove_tag_click);
        });
      }

     /*
      * Click event for a tag.
      */
      function remove_tag_click() {
        remove_tag(this);
        return false;
      }

      /*
      * During updating of the tags, we unbind the events to avoid
      * sideffects.
      */
      function unbind_taglist_events() {
        $(wrapper_sel+' '+tag_sel).each(function() {
          $(this).removeClass('processed');
          $(this).unbind('click',remove_tag_click);
          return false;
        }
        );
      }
    }
    );
  }
})(jQuery);
