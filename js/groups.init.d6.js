/**
 *
 * @author: Nicolás Notario (http://www.nivaria.com)
 * @Copyright 2012
 * Based on
 *  Author: Eugen Mayer (http://kontextwork.de)
 *  Copyright 2010
 *  http://drupal.org/project/tagging
 **/
(function ($) {
  Drupal.behaviors.groups_d6 = function() {
    var obj = {
      attach: function() {
        $('#edit-og-groups-wrapper:not(.grouping-processed)').groups(Drupal.settings.nice_groups);
      }
    };
    obj.attach();
  };
})(jQuery);
